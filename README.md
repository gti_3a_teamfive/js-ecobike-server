# JS EcoBike Server

Node.js API REST with express and mongoDB for ECOBike project

## Features

* Express
* REST API
* MongoDB

## Requirements

* [node & npm](https://nodejs.org/en/)

## Installation

* `git clone https://bitbucket.org/gti_3a_teamfive/js-ecobike-server.git`
* `npm install`
* `npm start`

### GET Routes

* visit http://localhost:3000
  * /users
  * /users/userId
