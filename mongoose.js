/* -------------------------------------------------------------------------- */
/*                        Connection to mongo database                        */
/* -------------------------------------------------------------------------- */

const mongoose = require("mongoose");
// const dbPath = "mongodb://mongo:27017/testdb";  // para el docker
const dbPath = process.env.DB_PATH;        // select develope database
//const dbPath = process.env.DB_PATH_TEST;   // select test database
var debug = require('debug')('js-ecobike-server:mongoose');

mongoose.connect(dbPath, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
});

const db = mongoose.connection;

/* ---------------------------- CONNECTION EVENTS --------------------------- */
// When successfully connected
db.on('connected', function () {
    debug('Mongoose default connection open to ' + dbPath);
});

// If the connection throws an error
db.on('error', function (err) {
    debug('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
db.on('disconnected', function () {
    debug('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection 
process.on('SIGINT', function () {
    db.close(function () {
        debug('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});
/* -------------------------------------------------------------------------- */

module.exports = mongoose;
