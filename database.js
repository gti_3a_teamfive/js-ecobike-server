/* -------------------------------------------------------------------------- */
/*                                 database.js                                */
/* -------------------------------------------------------------------------- */

const mongoose = require('./mongoose');
const User = require('./models/user');
const Measure = require('./models/measure');
const officialMeasure = require('./models/officialMeasure');
const interpolatedValue = require('./models/interpolatedValueModel');
var debug = require('debug')('js-ecobike-server:database');

/* ------------------------------ Get all users ----------------------------- */
exports.getAllUsers = function () {
    return new Promise(function (resolve, reject) {
        User.find()
            .then(users => {
                resolve(users);
            })
            .catch(err => {
                reject(err);
            })
    });
};
/* -------------------------------------------------------------------------- */

/* -------------------------- Get a particular user ------------------------- */
exports.getUser = function (userId) {
    return new Promise(function (resolve, reject) {
        User.findById(userId)
            .then(user => {
                resolve(user);
            })
            .catch(err => {
                reject(err);
            })
    });
};
/* -------------------------------------------------------------------------- */

/* ------------------------------ Save new user ----------------------------- */
exports.saveUser = function (userData) {
    return new Promise(function (resolve, reject) {
        const newUser = new User({
            name: userData.name,
            email: userData.email
        });

        newUser.save()
            .then(user => {
                resolve(user);
            })
            .catch(err => {
                reject(err);
            })
    });
};
/* -------------------------------------------------------------------------- */

/* ------------------------------ Get all measures ----------------------------- */
exports.getAllMeasures = function () {
    return new Promise(function (resolve, reject) {
        Measure.find()
            .then(measures => {
                resolve(measures);
            })
            .catch(err => {
                reject(err);
            })
    });
};
/* -------------------------------------------------------------------------- */

/* -------------------------- Get a particular measure ------------------------- */
exports.getMeasure = function (measureId) {
    return new Promise(function (resolve, reject) {
        Measure.findById(measureId)
            .then(measure => {
                resolve(measure);
            })
            .catch(err => {
                reject(err);
            })
    });
};
/* -------------------------------------------------------------------------- */

/* ------------------------------ Save new measure ----------------------------- */
exports.saveMeasure = function (measureData) {
    return new Promise(function (resolve, reject) {
        const newMeasure = new Measure({
            gasType: measureData.gasType,
            value: measureData.value,
            temperature: measureData.temperature,
            humidity: measureData.humidity,
            location: {
                type: "Point",
                coordinates: [measureData.longitude, measureData.latitude],
            },
            date: measureData.date,
            user: measureData.userId
        });

        newMeasure.save()
            .then(measure => {
                resolve(measure);
            })
            .catch(err => {
                reject(err);
            })
    });
};
/* -------------------------------------------------------------------------- */


//----------------------------------------------------------------------------------------
/* ------------------------------ Save new official measure ----------------------------- */
exports.saveOfficialMeasure = function (measureData) {
    return new Promise(function (resolve, reject) {
        
        officialMeasure.deleteMany({});
        const newMeasureo = new officialMeasure({
        
            so2: measureData.so2,
  no2: measureData.no2,
  co: measureData.co,
  no: measureData.no,
  nox: measureData.nox,
  o3: measureData.o3,
  date: measureData.date,
              location: {
                type: "Point",
                coordinates: [measureData.longitude, measureData.latitude],
            },
        });

        newMeasureo.save()
            .then(measure => {
                resolve(measure);
            })
            .catch(err => {
                reject(err);
            })
    });
};
/* -------------------------------------------------------------------------- */

/* ------------------------------ Get all official measures ----------------------------- */
exports.getOfficialMeasures = function () {
    return new Promise(function (resolve, reject) {
        officialMeasure.find()
            .then(measures => {
                resolve(measures);
            })
            .catch(err => {
                reject(err);
            })
    });
};
/* -------------------------------------------------------------------------- */

//----------------------------------------------------------------------------------------
/* ------------------------------ Save new interpolatedValue ----------------------------- 
exports.saveInterpolatedValue = function (measureData) {
    return new Promise(function (resolve, reject) {
        
       // officialMeasure.deleteMany({});
        const newMeasurev = new interpolatedValue({
        
            value: measureData.value,
              location: {
                type: "Point",
                coordinates: [measureData.lng, measureData.lat],
            },
            gasType: measureData.gasType
        });

        newMeasurev.save()
            .then(measure => {
                resolve(measure);
            })
            .catch(err => {
                reject(err);
            })
    });
};
 -------------------------------------------------------------------------- */

/* ------------------------------ Get one interpolated value ----------------------------- 
exports.getInterpolatedValue = function (lat, lng) {
    return new Promise(function (resolve, reject) {
        interpolatedValue.find({"location.coordinates":[lng, lat]})//"location.coordinates":[lng, lat]
            .then(measures => {
                resolve(measures);
            })
            .catch(err => {
                reject(err);
            })
    });
};
 -------------------------------------------------------------------------- */