/* -------------------------------------------------------------------------- */
/*                               business logic                               */
/* -------------------------------------------------------------------------- */

var db = require('./database');
const officialMeasure = require('./models/officialMeasure');
const Measure = require('./models/measure');
//------------------------------------------------------
// Import JSDOM


const jsdom = require("jsdom");


const {
    JSDOM
} = jsdom;


// Opciones para JSDOM

const options = {

    cookieJar: new jsdom.CookieJar() // Habilitar cookies

};





//-----------------------------------------------------

/* ---------------------- Creates a user and returns it --------------------- */
exports.createUser = function (userData) {
    return new Promise(function (resolve, reject) {
        if (!userData.name ||
            !userData.email) {
            reject(err);
            //reject('Missing fields');
            //return;
        }

        db.saveUser(userData)
            .then(user => {
                resolve(user);
            })
            .catch(err => {
                reject(err);
            })
    });
};
/* -------------------------------------------------------------------------- */

/* -------------------------- Get a particular user ------------------------- */
exports.readUser = function (userId) {
    return new Promise(function (resolve, reject) {
        if (!userId) {
            reject('Missing Id');
            return;
        }

        db.getUser(userId)
            .then(user => {
                resolve(user);
            })
            .catch(err => {
                reject(err);
            })
    });
};
/* -------------------------------------------------------------------------- */

/* ------------------------------ Get all users ----------------------------- */
exports.readAllUsers = function () {
    return new Promise(function (resolve, reject) {
        db.getAllUsers()
            .then(users => {
                resolve(users);
            })
            .catch(err => {
                reject(err);
            })
    });
};
/* -------------------------------------------------------------------------- */

/* ------------------------------- Update user ------------------------------ */
exports.updateUser = function (userData) {
    return new Promise(function (resolve, reject) {
        // comprobaciones

        db.setUser(userData)
            .then(user => {
                resolve(user);
            })
            .catch(err => {
                reject(err);
            })
    });
};
/* -------------------------------------------------------------------------- */

/* ------------------------ Delete a particular user ------------------------ */
exports.deleteUser = function (userId) {
    return new Promise(function (resolve, reject) {
        // comprobar que el usuario existe

        db.deleteUser(userId)
            .then(user => {
                resolve(user);
            })
            .catch(err => {
                reject(err);
            })
    });
};
/* -------------------------------------------------------------------------- */

/* ---------------------- Creates a measure and returns it --------------------- */
exports.createMeasure = function (measureData) {
    return new Promise(function (resolve, reject) {
        // comprobaciones de datos recibidos

        db.saveMeasure(measureData)
            .then(measure => {
                resolve(measure);
            })
            .catch(err => {
                reject(err);
            })
    });
};
/* -------------------------------------------------------------------------- */

/* -------------------------- Get a particular measure ------------------------- */
exports.readMeasure = function (measureId) {
    return new Promise(function (resolve, reject) {
        if (!measureId) {
            reject('Missing Id');
            return;
        }

        db.getMeasure(measureId)
            .then(measure => {
                resolve(measure);
            })
            .catch(err => {
                reject(err);
            })
    });
};
/* -------------------------------------------------------------------------- */

/* ------------------------------ Get all measures ----------------------------- */
exports.readAllMeasures = function () {
    return new Promise(function (resolve, reject) {
        db.getAllMeasures()
            .then(measures => {
                resolve(measures);
            })
            .catch(err => {
                reject(err);
            })
    });
};
/* -------------------------------------------------------------------------- */

/* ------------------------------- Update measure ------------------------------ */
exports.updateMeasure = function (measureData) {
    return new Promise(function (resolve, reject) {
        // comprobaciones

        db.setMeasure(measureData)
            .then(measure => {
                resolve(measure);
            })
            .catch(err => {
                reject(err);
            })
    });
};
/* -------------------------------------------------------------------------- */

/* ------------------------ Delete a particular measure ------------------------ */
exports.deleteMeasure = function (measureId) {
    return new Promise(function (resolve, reject) {
        // comprobar que la medida existe

        db.deleteMeasure(measureId)
            .then(measure => {
                resolve(measure);
            })
            .catch(err => {
                reject(err);
            })
    });
};
/* -------------------------------------------------------------------------- */


//-------------------------------------------------------------------------------------

/*-------------------------------------------------------------------------------------*/

/*--------------------------------- Obtain official measures ... by Víctor---------------------------*/
exports.officialMeasures = function () {

    // URLs de los datos

    const urlDatosGandia = "http://www.cma.gva.es/cidam/emedio/atmosfera/jsp/pde.jsp?PDE.CONT=912&estacion=5&titulo=46131002-Gandia&provincia=null&municipio=null&red=0&PDE.SOLAPAS.Mostrar=1111";

    const urlDatos = "http://www.cma.gva.es/cidam/emedio/atmosfera/jsp/datos_on_line.jsp";

    return new Promise(function (resolve, reject) {

        /*Estación de medida de Gandía:
 
 Longitud   0º 11' 25'' Oeste
Latitud   38º 58' 08'' Norte*/
        var posicionEstacion = {
            latitude: 38.96888889,

            longitude: -0.19027778
        }

        Measure.find({
            "location": {
                "$near": {
                    "$geometry": {
                        "type": "Point",
                        "coordinates": [posicionEstacion.longitude, posicionEstacion.latitude]
                    },
                    $maxDistance: 5000,
                    $minDistance: 100
                }
            }
        }).then(nearbyMeasures => {

            (async function () {

                // Array donde guardaremos las medidas

                var data = [];


                // Primera petición para obtener las cookies

                await JSDOM.fromURL(urlDatosGandia, options);


                // Petición para obtener datos contaminación

                await JSDOM.fromURL(urlDatos, options).then(dom => {




                    // Tabla de contaminación

                    let table = dom.window.document.getElementsByTagName("table")[7];


                    // Filas de la tabla
                    var temperatura = nearbyMeasures[0].temperature
                    let rows = table.getElementsByTagName("tr");


                    if (rows.length != 0) {
                        officialMeasure.deleteMany({}, (err) => {

                        });
                    }


                    /*The general equation is
µg/m3
 = (ppb)*(12.187)*(M) / (273.15 + °C)
where M is the molecular weight of the gaseous pollutant. An atmospheric pressure of 1
atmosphere is assumed.*/

                    var conversionAppb = function (mgOug, pesoMolecular) {
                        //273.15 + 19
                        return Math.round(Number(mgOug) * (24.45) * 1000 / (pesoMolecular))
                    }





                    // Recorremos filas

                    for (i = 1; i < rows.length; i++) {

                        // Por cada fila, montamos un objeto medida




                        let measure = {



                            date: rows[i].children[0].innerHTML,

                            so2: conversionAppb(rows[i].children[1].innerHTML, 64.02),

                            co: conversionAppb(rows[i].children[3].innerHTML, 28.01),

                            no: conversionAppb(rows[i].children[5].innerHTML, 30.01),

                            no2: conversionAppb(rows[i].children[6].innerHTML, 46.01),

                            nox: conversionAppb(rows[i].children[8].innerHTML, 41.3),

                            o3: conversionAppb(rows[i].children[9].innerHTML, 48),

                            latitude: posicionEstacion.latitude,

                            longitude: posicionEstacion.longitude

                        }

                        // Guardamos objeto en el array de medidas

                        db.saveOfficialMeasure(measure).then(measure => {

                            })
                            .catch(err => {
                                reject(err);
                            }) //
                        data.push(measure);

                    } //for

                }) //await JSDOM

                // Mostramos array medidas


                //console.log(JSON.stringify(data));
                //resolve(data);

            }())

            db.getOfficialMeasures()
                .then(measures => {
                    resolve(measures);
                })
                .catch(err => {
                    reject(err);
                })
        }); //Promise

    }).catch(err => {
        throw (err);
    }) //encontrar las medidas cercanas

} //officialMeasures()

//------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
/*-------------------------------------------------------------------------------------*/

/*--------------------------------- Obtain the interpolated value  ---------------------------*/
// Autor: V. Blanco
//   R, R, Text -> interpolatedValue() -> N
// ---------------------------------------------

exports.interpolatedValue = function (lat, lng, gasType) {

    return new Promise(function (resolve, reject) {

        //
        // look for all the rnearby measures in a 1km radius
        //
        Measure.find({
                "location": {
                    "$near": {
                        "$geometry": {
                            "type": "Point",
                            "coordinates": [lng, lat]
                        },
                        $maxDistance: 1000,
                        $minDistance: 10
                    }
                }
            }).then(nearbyMeasures => {

                // no hi ha cap mesura
                if (nearbyMeasures.length == 0) {
                    //if no near measurement is found, return the station measured value
                getOfficialMeasures(lat,lng,gasType, (measure)=>{resolve(measure)})

                } else {
                    //if some measure is found, return the interpolated value from at most 4 different measures 
                interpolate(nearbyMeasures,lat,lng,gasType, (value)=>{resolve(value)})
                   } //if else
                
            })
            .catch(err => {
                reject(err);
            })
    }).catch(err => {
        throw (err);
    })
} //interpolatedValue

//-----------------------------------------------------------------------
// nearbyMeasures: lista<measure>,lat,lng,gasType -->interpolate()--> interpolated value
// author: Víctor
//-----------------------------------------------------------------------
function interpolate(nearbyMeasures, lat,lng,gasType, callback){
     var nearestMeasures = []
                    var countFour = 4;
                    var distanceDivisor = 0;
                    var interpolatedValue = 0;

               //if the gastype is "*", return the sum of all the pollution of the four nearest measures(every gas)
                    if (gasType == "*") {
                        for (let k = 0; k < nearbyMeasures.length; k++) {

                            if (!countFour) break;


                            interpolatedValue += nearbyMeasures[k].value;
                            countFour--;

                        } //for
                        let measure = {value: interpolatedValue,lat: lat, lng: lng, gasType: gasType }
                        callback(measure);
                        
                    //if the gas type is not "*" we calculate the distance weighted mean according to the gasType    
                    } else {
                        //now we will calculate the total amount of distance to weight each measured value in function of its distance to the point
                        for (let k = 0; k < nearbyMeasures.length; k++) {

                            if (!countFour) break;

                            if (nearbyMeasures[k].gasType == gasType) {
                                distanceDivisor += getDistanceFromLatLonInKm(nearbyMeasures[k].location.coordinates[1], nearbyMeasures[k].location.coordinates[0], lat, lng);
                                nearestMeasures.push(nearbyMeasures[k])
                                countFour--;
                            }
                        } //for

                        //and then do the weighted value process 
                        if (!nearestMeasures.length) {

                            //if there are not nearest measures of the selected gas the station value is returned
                         getOfficialMeasures(lat,lng,gasType, (measure)=>{callback(measure)})
                     } else {

                            for (let l = 0; l < nearestMeasures.length; l++) {
                               interpolatedValue += (getDistanceFromLatLonInKm(nearestMeasures[nearestMeasures.length - l - 1].location.coordinates[1], nearestMeasures[nearestMeasures.length - l - 1].location.coordinates[0], lat, lng) * (nearestMeasures[l].value) / distanceDivisor)
                            } //for
                     let measure = {value: interpolatedValue,lat: lat, lng: lng, gasType: gasType }
                            callback(measure)
                        } //weighting                        
                    }//else
}//interpolate


//-----------------------------------------------------------------------
// lat,lng,gasType -->getOfficialMeasures()--> official measure from the given gas
// author: Víctor
//-----------------------------------------------------------------------
function getOfficialMeasures(lat,lng, gasType,callback){
        db.getOfficialMeasures()
                                .then(medidas => {
                                    //resolve(measures);

                                    // find the latest measures
                                    var latestHour = 0;
                                    var latestMeasures = {};
                                    var totalPollution = 0;

                                    for (let i = 0; i < medidas.length; i++) {
                                        if (Number(medidas[i].date.split(":")[0]) >= latestHour) {

                                            latestHour = Number(medidas[i].date.split(":")[0]);

                                            latestMeasures = medidas[i];
                                        } else if (Number(medidas[i].date.split(":")[0]) == 0) {
                                            latestHour = Number(medidas[i].date.split(":")[0]);

                                            latestMeasures = medidas[i];
                                        }
                                    }
                                    switch (gasType) {
                                        case "CO":
                                            totalPollution = latestMeasures.co
                                            break;
                                        case "SO2":
                                            totalPollution = latestMeasures.so2
                                            break;
                                        case "NO3":
                                            totalPollution = latestMeasures.no
                                            break;
                                        case "O3":
                                            totalPollution = latestMeasures.o3
                                            break;
                                        case "*":
                                            totalPollution = latestMeasures.co + latestMeasures.so2 + latestMeasures.o3 + latestMeasures.no;
                                            break;
                                    }

                                    if (totalPollution == null) {
                                        totalPollution = 0
                                    }

                                    let measure = {
                                        value: totalPollution,
                                        lat: lat,
                                        lng: lng,
                                        gasType: gasType

                                    }

                                    callback(measure);

                                }) //db.getOfficialMeasures()
}//getOfficialMeasure()


//---------------------------------------------------------------
// lat1: R, lng1:R, lat2:r, lng2:R -->getDistanceFromLatLonInKm()-->distance:R
// author: Víctor
//----------------------------------------------------------------
function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1); // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
}


//-----------------------------------------------------------------
// degrees: R--> drg2rad() --> radians:R
// author: Víctor
//-------------------------------------------------------------------
function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

/*

var db = require('../db')

// Create new comment in your database and return its id

exports.create = function(user, text, cb) {
  var comment = {
    user: user,
    text: text,
    date: new Date().toString()
  }

  db.save(comment, cb)
}

// Get a particular comment

exports.get = function(id, cb) {
  db.fetch({id:id}, function(err, docs) {
    if (err) return cb(err)
    cb(null, docs[0])
  })
}

// Get all comments

exports.all = function(cb) {
  db.fetch({}, cb)
}

// Get all comments by a particular user

exports.allByUser = function(user, cb) {
  db.fetch({user: user}, cb)
} */
