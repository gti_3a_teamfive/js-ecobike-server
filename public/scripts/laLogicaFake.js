// -------------------------------------------
// LaLogicaFake.js
// Clase encargada de hacer peticiones al servidor
// equipo 5
// autor: Víctor Blanco Bataller
// 3/11/2019
// copyright
// -------------------------------------------



export class laLogicaFake {


    constructor() {
        this.urlServidor = "http://localhost:3000";
    }


    //--------------------------------------------------
    // obtenerMedidas()-->Lista <Medida>
    //método que obtiene las medidas de la base de datos
    //----------------------------------------------------------

    obtenerMedidas(callback) {


        fetch(this.urlServidor + "/measures")
            .then(function (response) {

                return response.json();

            })
            .then((medidas) => {

                callback(medidas);

            }) //fetch

    } //obtenerMedidas()

    //--------------------------------------------------
    // obtenerMedidasOficiales()-->Lista <Medida>
    //método que obtiene las medidas oficiales de la estacion de Gandía
    //----------------------------------------------------------

    obtenerMedidasOficiales(callback) {


        fetch(this.urlServidor + "/officialMeasures")
            .then(function (response) {

                return response.json();

            })
            .then((medidas) => {

                var latestHour = 0;
                var latestMeasures = {};

                for (let i = 0; i < medidas.length; i++) {

                    if (Number(medidas[i].date.split(":")[0]) >= latestHour) {

                        latestHour = Number(medidas[i].date.split(":")[0]);

                        latestMeasures = medidas[i];
                    } else if (Number(medidas[i].date.split(":")[0]) == 0) {
                        latestHour = Number(medidas[i].date.split(":")[0]);

                        latestMeasures = medidas[i];
                    }
                }
                callback(latestMeasures);

            }) //fetch

    } //obtenerMedidas()


    //--------------------------------------------------
    // obtenerPosicionDeEstacion()-->Posicion:{Lat, Long}
    //método que obtiene la posición de la estacion de Gandía
    //----------------------------------------------------------
    obtenerPosicionDeEstacion(callback) {


        fetch(this.urlServidor + "/officialMeasures/position")
            .then(function (response) {

                return response.json();

            })
            .then((posicion) => {

                callback(posicion);

            }) //fetch
    }



    //--------------------------------------------------
    // Posicion:{Lat, Long}, tipoDeGas:texto--> valorInterpolado()-->Posicion:{Lat, Long}, valor: R, tipoDeGas:R
    //método que obtiene el valor interpolado de un punto dado
    //----------------------------------------------------------
    valorInterpolado(lat, lng, tipoDeGas, callback) {


        var peticion = {
            lat: lat,
            lng: lng,
            gasType: tipoDeGas
        }
        var formBody = [];
        for (var property in peticion) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(peticion[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");


        fetch(this.urlServidor + "/interpolatedValue", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: formBody

            }).then(function (response) {

                return response.json();

            })
            .then((valor) => {

                callback(valor);

            }) //fetch
    } //valorInterpolado
    
    
    
       //--------------------------------------------------
    // Posicion1:{Lat, Long}, Posicion2:{Lat,Long}--> route()-->Ruta(es una lista de puntos con propiedades)
    //método que obtiene la ruta menos contaminada entre dos puntos
    //----------------------------------------------------------
    
    route(waypoints, callback, context, options){
        
        
           var peticion = {
            latS: waypoints[0].latLng.lat,
            lngS: waypoints[0].latLng.lng,
           latE: waypoints[1].latLng.lat,
            lngE: waypoints[1].latLng.lng,
        }
        var formBody = [];
        for (var property in peticion) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(peticion[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");
        
        
        fetch(this.urlServidor + "/createRoute", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: formBody

            }).then(function (response) {

                return response.json();

            })
            .then((valor) => {

                callback(valor);

            }) //fetch
    }
    
    
    
    
}
