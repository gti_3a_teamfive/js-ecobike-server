  // -------------------------------------------
  // mapa.js
  // Script encargado de hacer funcional el mapa que se visualizará en la app y la web
  // equipo 5
  // autor: Víctor Blanco Bataller
  // 3/11/2019
  // copyright
  // -------------------------------------------


  import {
      creadorDeMapa
  } from './creadorDeMapa.js'



  // inicializamos el objeto

  var mapaCreado = new creadorDeMapa();

  var posicionActual;
  var precisionActual;

  //--------------------------------------------------
    // estoyLocalizado()
    //función que se llamará cuando esté localizado
    //----------------------------------------------------------
  function estoyLocalizado(e) {

      // si la posicion está definida, borra el marcador y el círculo de posición anteriores
      if (posicionActual) {
          mapaCreado.mimapa.removeLayer(posicionActual);
          mapaCreado.mimapa.removeLayer(precisionActual);
      }else{
          posicionActual= [38.996395, -0.166339];
      }

      var radius = e.accuracy / 2;

      posicionActual = L.marker(e.latlng).addTo(mapaCreado.mimapa)
          .bindPopup("You are within " + radius + " meters from this point");
      mapaCreado.setPosicion(posicionActual);

      precisionActual = L.circle(e.latlng, radius).addTo(mapaCreado.mimapa);
  }



 //--------------------------------------------------
    // errorDeLocalizacion()
    //función que se llamará cuando haya un error de localización
    //----------------------------------------------------------
  function errorDeLocalizacion(e) {
      alert(e.message);
       posicionActual = L.marker([38.996395, -0.166339]).addTo(mapaCreado.mimapa)
          .bindPopup("Estas sobre " + 300 + " metros desde este punto");
       precisionActual = L.circle([38.996395, -0.166339], 300).addTo(mapaCreado.mimapa);
mapaCreado.mimapa.setView([38.996395, -0.166339], 13);
  }


// Añadimos las funciones anteriores a escuchadores de evento del mapa




	 var gasSeleccionado="CO";
      var capaSeleccionada = mapaCreado.CO
      

	     function createButton(label, container) {
    var btn = L.DomUtil.create('button', '', container);
    btn.setAttribute('type', 'button');
    btn.innerHTML = label;
    return btn;
}

mapaCreado.mimapa.on('click', function(e) {
   // console.log(e.latlng)
    mapaCreado.valorInterpolado(e.latlng.lat,e.latlng.lng, gasSeleccionado,(valor)=>{
           console.log(valor)
         L.popup()
    .setContent("<b>" + gasSeleccionado + ": </b><br />" + valor.value.toString() + "ppb").setLatLng([valor.lat, valor.lng]).openOn(mapaCreado.mimapa);
    })
    
   
    
 /*
    var container = L.DomUtil.create('div'),
       // valor=createButton(valor.toString(), container),
        startBtn = createButton('Empezar desde aquí', container),
        destBtn = createButton('Ir hasta aquí', container);

    L.popup()
        .setContent(container)
        .setLatLng(e.latlng)
        .openOn(mapaCreado.mimapa);
    
      L.DomEvent.on(startBtn, 'click', function() {
        mapaCreado.control.spliceWaypoints(0, 1, e.latlng);
        mapaCreado.mimapa.closePopup();
            
    });
        
    
        L.DomEvent.on(destBtn, 'click', function() {
        mapaCreado.control.spliceWaypoints(mapaCreado.control.getWaypoints().length - 1, 1, e.latlng);
        mapaCreado.mimapa.closePopup();
    });
    
    */
    
});

	
  ///mapaCreado.mimapa.on('click', onMapClick);

  mapaCreado.mimapa.setView([38.996395, -0.166339],13);
    
      mapaCreado.mimapa.on('baselayerchange', function (e) {
          capaSeleccionada=e.layer
          if(e.layer==mapaCreado.CO){
             gasSeleccionado="CO"; 
          }else if(e.layer==mapaCreado.SO2){
             gasSeleccionado="SO2"; 
          }else if(e.layer==mapaCreado.NO3){
             gasSeleccionado="NO3"; 
          }else if(e.layer==mapaCreado.O3){
             gasSeleccionado="O3"; 
          }
 
});

  mapaCreado.mimapa.on('locationfound', estoyLocalizado);
  mapaCreado.mimapa.on('locationerror', errorDeLocalizacion);
//setTimeout(()=>{mapaCreado.mimapa.on('zoomstart', ()=>{mapaCreado.ajustarRadio()})}, 1000)
   






/*

// don't forget to include leaflet-heatmap.js
var testData = {
  max: 8,
  data: [{lat: 24.6408, lng:46.7728, count: 3},{lat: 50.75, lng:-1.55, count: 1}]
};

var baseLayer = L.tileLayer(
  'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
    attribution: '...',
    maxZoom: 18
  }
);

var cfg = {
  // radius should be small ONLY if scaleRadius is true (or small radius is intended)
  // if scaleRadius is false it will be the constant radius used in pixels
  "radius": 2,
  "maxOpacity": .8,
  // scales the radius based on map zoom
  "scaleRadius": true,
  // if set to false the heatmap uses the global maximum for colorization
  // if activated: uses the data maximum within the current map boundaries
  //   (there will always be a red spot with useLocalExtremas true)
  "useLocalExtrema": true,
  // which field name in your data represents the latitude - default "lat"
  latField: 'lat',
  // which field name in your data represents the longitude - default "lng"
  lngField: 'lng',
  // which field name in your data represents the data value - default "value"
  valueField: 'count'
};


var heatmapLayer = new HeatmapOverlay(cfg);

var map = new L.Map('map-canvas', {
  center: new L.LatLng(25.6586, -80.3568),
  zoom: 4,
  layers: [baseLayer, heatmapLayer]
});

heatmapLayer.setData(testData);*/

  //posicion estacion de medicion Gandia: 38.9688889, -0.19027777777777777
  //----------------------------------------------------



  //var url ='https://www.eltiempo.es/calidad-aire/valencia#pollution_table';

  // call locate every 3 seconds... forever
  // window.setInterval(locate, 3000)
