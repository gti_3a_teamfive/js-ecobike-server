// -------------------------------------------
// creadorDeMapa.js
// clase encargada de crear el mapa, añadirle los marcadores de las medidas filtradas
// equipo 5
// autor: Víctor Blanco Bataller
// 3/11/2019
// copyright
// -------------------------------------------


import {
    laLogicaFake
} from './laLogicaFake.js'



export class creadorDeMapa {


    mimapa;

    // constructor

    constructor() {

        this.posicion = [38.996395, -0.166339];

        this.laLogicaFake = new laLogicaFake();

        this.inicializarMapa();

        this.inicializarUmbrales();

        this.crearIconos();
        
           this.visualizarEstacionDeMedida();
 
          this.valorInterpolado(38.96888889, -0.19027778, "SO2",(valor)=>{console.log(valor)});
    

        this.crearCapas();
        
   
        
        var that = this;
        setTimeout(function () {
            that.crearRuta([38.96888889, -0.19027778])
            that.control.addTo(that.mimapa)
              that.obtenerMedidas();
        }, 1000)

    }

    //--------------------------------------------------
    // getPosicion()
    //método que devuelve la posicion
    //----------------------------------------------------------
    getPosicion() {
        return this.posicion;
    }

    //--------------------------------------------------
    //Posicion-->
    // getPosicion()
    //método para actualizar la posicion
    //----------------------------------------------------------
    setPosicion(posicion) {
        this.posicion = posicion;
        // this.crearRuta(this.posicion);
    }

    //--------------------------------------------------
    // inicializarMapa()
    //método que inicializa los grupos de medidas y los basemaps y crea el mapa
    //----------------------------------------------------------
    inicializarMapa() {

        //Creamos los grupos de medidas de gases

        this.CO = L.layerGroup();

        this.NO3 = L.layerGroup();

        this.SO2 = L.layerGroup();

        this.O3 = L.layerGroup();
        
        
        
        
        
        var testData = {
            
  max:200000 ,
  data: [{latitud: 24.6408, longitud:46.7728, valor: 3},{latitud: 50.75, longitud:-1.55, valor: 1}]
}; 
        
              var cfgCO = {
  // radius should be small ONLY if scaleRadius is true (or small radius is intended)
  // if scaleRadius is false it will be the constant radius used in pixels
  "radius": 0.003,
  "maxOpacity": .4,
  // scales the radius based on map zoom
  "scaleRadius": true,
  // if set to false the heatmap uses the global maximum for colorization
  // if activated: uses the data maximum within the current map boundaries
  //   (there will always be a red spot with useLocalExtremas true)
  "useLocalExtrema": false,
  // which field name in your data represents the latitude - default "lat"
  latField: 'latitud',
  // which field name in your data represents the longitude - default "lng"
  lngField: 'longitud',
  // which field name in your data represents the data value - default "value"
  valueField: 'valor',
                    blur: .99,
  gradient: {
    // enter n keys between 0 and 1 here
    // for gradient color customization
    '.2': 'lime',
    '.87': 'yellow',
    '1': 'red'
  }
};
             var testData2 = {
              
  max:200000 ,
  data: [{latitud: 24.6408, longitud:46.7728, valor: 3},{latitud: 50.75, longitud:-1.55, valor: 1}]
}; 
        
              var cfgSO2 = {
  // radius should be small ONLY if scaleRadius is true (or small radius is intended)
  // if scaleRadius is false it will be the constant radius used in pixels
  "radius": 0.003,
  "maxOpacity": .4,
  // scales the radius based on map zoom
  "scaleRadius": true,
  // if set to false the heatmap uses the global maximum for colorization
  // if activated: uses the data maximum within the current map boundaries
  //   (there will always be a red spot with useLocalExtremas true)
  "useLocalExtrema": false,
  // which field name in your data represents the latitude - default "lat"
  latField: 'latitud',
  // which field name in your data represents the longitude - default "lng"
  lngField: 'longitud',
  // which field name in your data represents the data value - default "value"
  valueField: 'valor',
                    blur: .99,
  gradient: {
    // enter n keys between 0 and 1 here
    // for gradient color customization
    '.2': 'lime',
    '.87': 'yellow',
    '1': 'red'
  }
};
         var testData3 = {
              
  max:200000 ,
  data: [{latitud: 24.6408, longitud:46.7728, valor: 3},{latitud: 50.75, longitud:-1.55, valor: 1}]
}; 
        
              var cfgNO3 = {
  // radius should be small ONLY if scaleRadius is true (or small radius is intended)
  // if scaleRadius is false it will be the constant radius used in pixels
  "radius": 0.003,
  "maxOpacity": .4,
  // scales the radius based on map zoom
  "scaleRadius": true,
  // if set to false the heatmap uses the global maximum for colorization
  // if activated: uses the data maximum within the current map boundaries
  //   (there will always be a red spot with useLocalExtremas true)
  "useLocalExtrema": false,
  // which field name in your data represents the latitude - default "lat"
  latField: 'latitud',
  // which field name in your data represents the longitude - default "lng"
  lngField: 'longitud',
  // which field name in your data represents the data value - default "value"
  valueField: 'valor',
                    blur: .99,
  gradient: {
    // enter n keys between 0 and 1 here
    // for gradient color customization
    '.2': 'lime',
    '.87': 'yellow',
    '1': 'red'
  }
};
        
         var testData4 = {
              
  max:200000 ,
  data: [{latitud: 24.6408, longitud:46.7728, valor: 3},{latitud: 50.75, longitud:-1.55, valor: 1}]
}; 
        
              var cfgO3 = {
  // radius should be small ONLY if scaleRadius is true (or small radius is intended)
  // if scaleRadius is false it will be the constant radius used in pixels
  "radius": 0.003,
  "maxOpacity": .4,
  // scales the radius based on map zoom
  "scaleRadius": true,
  // if set to false the heatmap uses the global maximum for colorization
  // if activated: uses the data maximum within the current map boundaries
  //   (there will always be a red spot with useLocalExtremas true)
  "useLocalExtrema": false,
  // which field name in your data represents the latitude - default "lat"
  latField: 'latitud',
  // which field name in your data represents the longitude - default "lng"
  lngField: 'longitud',
  // which field name in your data represents the data value - default "value"
  valueField: 'valor',
                    blur: .99,
  gradient: {
    // enter n keys between 0 and 1 here
    // for gradient color customization
    '.2': 'lime',
    '.87': 'yellow',
    '1': 'red'
  }
};
        
     this.calorCO = new HeatmapOverlay(cfgCO);
           this.calorSO2 = new HeatmapOverlay(cfgSO2);
          this.calorNO3 = new HeatmapOverlay(cfgNO3);
        this.calorO3 = new HeatmapOverlay(cfgO3);
        
       // this.heatmapLayer= L.layerGroup();

        //Creamos el mapa:

        this.mimapa = L.map('idmapa', {


           layers: [this.CO]// layers: [this.NO3, this.CO, this.SO2, this.O3]
        });
        
        this.calorCO.setData(testData);
        this.calorSO2.setData(testData2);
        this.calorNO3.setData(testData3);
        this.calorO3.setData(testData4);
        
       

        //vista de calles        

        this.calles = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            maxZoom: 30,
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            id: 'mapbox.streets'
        })






        var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            mbUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';


        //vista de satelite
        this.satelite = L.tileLayer(mbUrl, {

            id: 'mapbox.satellite',
            attribution: mbAttr
        })


        //this.satelite.addTo(this.mimapa);
        //El mapa visualizará las calles por defecto

        this.calles.addTo(this.mimapa);
         this.localizar()


    // carga el control de expoasición
    var exposicion = new L.Control.Exposicion({
        position: 'bottomright'
    }).addTo(this.mimapa);

    } //inicializarMapa()




    //--------------------------------------------------
    // inicializarUmbrales()
    //método que inicializa los umbrales de cada gas
    //----------------------------------------------------------
    inicializarUmbrales() {

        //Definimos los límites de NO3 (en ppb)

        this.limiteBuenoNO3 = 50000;
        this.limiteModeradoNO3 = 200000;
        //this.imiteMaloNO3= 400000;

        //Definimos los límites de CO (en ppb, segun los niveles de las alarmas de la empresa Kidde):

        this.limiteBuenoCO = 50000;
        this.limiteModeradoCO = 200000;
        //this.limiteMaloCO= 400000;


        //Definimos los límites de SO2(en ppb)

        this.limiteBuenoSO2 = 50000;
        this.limiteModeradoSO2 = 200000;
        // this.imiteMaloSO2= 400000;

        //Definimos los límites de O3(en ppb)

        this.limiteBuenoO3 = 50000;
        this.limiteModeradoO3 = 200000;
        //this.limiteMaloO3= 400000;

    } //inicializarUmbrales()



    //--------------------------------------------------
    // crearIconos()
    //método que crea los iconos
    //----------------------------------------------------------
    crearIconos() {
        //Creamos los iconos:                       

        this.iconoBueno = L.icon({
            iconUrl: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
            shadowUrl: 'http://maps.gstatic.com/mapfiles/shadow50.png',
        });
        this.iconoModerado = L.icon({
            iconUrl: 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png',
            shadowUrl: 'http://maps.gstatic.com/mapfiles/shadow50.png',
        });

        this.iconoHorrible = L.icon({
            iconUrl: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png',
            shadowUrl: 'http://maps.gstatic.com/mapfiles/shadow50.png',
        });
        this.iconoOficial = L.icon({
            iconUrl: 'http://maps.google.com/mapfiles/ms/icons/purple-dot.png',
            shadowUrl: 'http://maps.gstatic.com/mapfiles/shadow50.png',
        });
        this.iconoDestino = L.icon({
            iconUrl: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
            shadowUrl: 'http://maps.gstatic.com/mapfiles/shadow50.png',
        });
    } //inicializarIconos()


    //--------------------------------------------------
    // localizar()
    //método que localiza al usuario cuando la posición cambia
    //----------------------------------------------------------
    localizar() {
        var zoomDeAhora = this.mimapa.zoom
        this.mimapa.locate({
            timeout:1000,
           // setView:true,
            setZoom: zoomDeAhora,
            watch: true,
            enableHighAccuracy: true
        });
      
    } //localizar()


    //--------------------------------------------------
    // obtenerMedidas()
    //método que filtra las medidas, las añade a su capa correspondiente y crea marcadores en  su posición
    //----------------------------------------------------------
    obtenerMedidas() {

        //borramos las anteriores medidas
        this.CO.clearLayers();
        this.SO2.clearLayers();
        this.NO3.clearLayers();
        this.O3.clearLayers();
        //--------------------------------------------------------------------------------
    this.listaNO3 = []
        this.listaO3 = [];
        this.listaSO2 = [];
        this.listaCO = [];
        //---------------------------------------------------------------------------------
        this.laLogicaFake.obtenerMedidas((medidas) => {
            
            medidas.forEach(medida => {

                //Obtenemos posiciones
                var latitud = medida.location.coordinates[1]; //38.996001+(Math.floor(Math.random() * 199999) - 99999)*0.0000001;
                var longitud = medida.location.coordinates[0]; //-0.160066+(Math.floor(Math.random() * 199999) - 99999)*0.0000001;


                //Medida real
                var valor = medida.value; //Math.floor(Math.random() * 500000)+ 5000;

                var icono;

                var capa;
                var limiteBueno;
                var limiteModerado;

                //comprobamos qué gas es para saber a qué capa añadir la medida y qué límites usar:
                if (medida.gasType == "NO3") {
                    limiteBueno = this.limiteBuenoNO3;
                    limiteModerado = this.limiteModeradoNO3;
                    //------------------------------------------------------------------------------------------------------

                    this.listaNO3.push({"latitud":Number(latitud), "longitud":Number(longitud), "valor":Number(valor)}); //50
                    this.listaNO3.push(this.NO3oficial);
                    //------------------------------------------------------------------------------------------------------
                    capa = this.NO3
                }
                if (medida.gasType == "O3") {
                    limiteBueno = this.limiteBuenoO3;
                    limiteModerado = this.limiteModeradoO3;
                    //------------------------------------------------------------------------------------------------------

                    this.listaO3.push({"latitud":Number(latitud), "longitud":Number(longitud), "valor":Number(valor)}); //50
                     this.listaO3.push(this.O3oficial);
                    //------------------------------------------------------------------------------------------------------
                    capa = this.O3
                }
                if (medida.gasType == "SO2") {
                    limiteBueno = this.limiteBuenoSO2;
                    limiteModerado = this.limiteModeradoSO2;
                    //------------------------------------------------------------------------------------------------------

                    this.listaSO2.push({"latitud":Number(latitud), "longitud":Number(longitud), "valor":Number(valor)}); //50
                    this.listaSO2.push(this.SO2oficial)
                    //------------------------------------------------------------------------------------------------------
                    capa = this.SO2;
                }
                if (medida.gasType == "CO") {
                    limiteBueno = this.limiteBuenoCO;
                    limiteModerado = this.limiteModeradoCO;
                    //------------------------------------------------------------------------------------------------------

                    this.listaCO.push({"latitud":Number(latitud), "longitud":Number(longitud), "valor":Number(valor)}); //50
                    //this.listaCO.push(this.COoficial)
                    //------------------------------------------------------------------------------------------------------
                    capa = this.CO
                }

                //comprobamos qué icono usar:     
                if (valor < limiteBueno) {
                    icono = this.iconoBueno;
                } else {
                    if (valor < limiteModerado) {
                        icono = this.iconoModerado;
                    } else {

                        icono = this.iconoHorrible;

                    }
                }
                // Creamos el marcador
               // this.crearMarcadorMedida(latitud, longitud, icono, medida.gasType, valor, capa);
                

                //--------------------------------------------------------------------------------
               

                //--------------------------------------------------------------------------------
            }) //forEach

            //--------------------------------------------------------------------------------
            this.Interpolar(this.listaCO,this.calorCO, this.CO,this.limiteModeradoCO);
            this.Interpolar(this.listaSO2, this.calorSO2, this.SO2,this.limiteModeradoSO2);
             this.Interpolar(this.listaNO3, this.calorNO3, this.NO3,this.limiteModeradoNO3);
             this.Interpolar(this.listaO3, this.calorO3, this.O3,this.limiteModeradoO3);
           
            //--------------------------------------------------------------------------------

           // this.coropletas(medidas);
        })



    } //popularConTodasLasMedidas()


    
    
    //-----------------------------------------------------------------------------------------
    //lista<puntos([lat. long, valor])>, capa-->
    // Interpolar()
    // método que crea un mapa de calor a partir de unos puntos dados y lo genera en una capa sobreponible al mapa que le viene dada
    //------------------------------------------------------------------------------------------------------
    Interpolar(puntos, capaCalor, capa, max) {
        console.log(puntos)
       
        
        var testData = {
            
  max: max,
  data: puntos
};
  
        capaCalor.addTo(capa)
        
        capaCalor.setData(testData);
        
       /* console.log(this.calorCO._heatmap.getValueAt({y:-0.18903136253356936,
                                x:  38.9782 }))*/
    } //Interpolar()


        //-----------------------------------------------------------------------------------------
    //Latitud:R, longitud:R, icono: icono, tipoDeGas: String, valor: R, capa-->
    // crearMarcadorMedida()
    // método que crea un marcador en el mapa en la posición especificada con un popup que indica el tipo de gas y la medida
    //------------------------------------------------------------------------------------------------------
    crearMarcadorMedida(latitud, longitud, icono, tipoDeGas, valor, capa) {
        
L.marker([latitud, longitud], {
                        icon: icono
                    })
                    .bindPopup("<b>" + tipoDeGas + ": </b><br />" + valor.toString() + "ppb").openPopup().addTo(capa);
    } //crearMarcadorMedida()
    
    
    
    
    //--------------------------------------------------
    //lat:R, lng:R, gas:texto--> valorInterpolado()-->valorInterpolado:N
    //método que devuelve el valor interpolado de un gas dado en un punto concreto
    //----------------------------------------------------------
    valorInterpolado(lat, long, gas,callback){
         this.laLogicaFake.valorInterpolado(lat, long, gas, (valor) => {
             
             callback(valor);
         })
    }
    
    
    
    
    
    
    //--------------------------------------------------
    // visualizarEstacionDeMedida()
    //método que crea el marcador en la posición de la estación de medida de Gandía y permite visualizar sus valores
    //----------------------------------------------------------

    visualizarEstacionDeMedida() {

        this.laLogicaFake.obtenerPosicionDeEstacion(posicion => {


            this.laLogicaFake.obtenerMedidasOficiales((medidas) => {

                var latitudOficial = posicion.latitude

                if (posicion.longitude > 0) posicion.longitude = -posicion.longitude

                var longitudOficial = posicion.longitude

                var COoficial = medidas.co;

                this.COoficial={latitud:latitudOficial, longitud:longitudOficial, valor:COoficial}

                var SO2oficial = medidas.so2;

                this.SO2oficial={latitud:latitudOficial,longitud:longitudOficial, valor: SO2oficial}

                var NO3oficial = medidas.nox;

               this.NO3oficial={latitud:latitudOficial, longitud:longitudOficial, valor: NO3oficial}

                var O3oficial = medidas.o3;

                this.O3oficial={latitud:latitudOficial, longitud:longitudOficial, valor:O3oficial}

                var estacion = L.marker([latitudOficial, longitudOficial], {
                        icon: this.iconoOficial
                    })
                    .bindPopup("<b>CO: </b><br />" + COoficial.toString() + "ppb<br /><b>SO2: </b><br />" + SO2oficial.toString() + "ppb<br /><b>NO3: </b><br />" + NO3oficial.toString() + "ppb<br /><b>O3: </b><br />" + O3oficial.toString()).openPopup().addTo(this.mimapa);

            }) //obtenerMedidasOficiales()

        }) //obtenerPosicionDeLaEstacion()
    } // visualizarEstacionDeMedida()



    //--------------------------------------------------
    // crearRuta()
    //método que crea una ruta desde la posición a la estación de medidas la primera vez 
    // y luego crea rutas entre dos puntos que esquivan la contaminación
    //----------------------------------------------------------
    crearRuta(destino) {

       // variable usada para almacenar el estado inicial del control
        var controlAux;
        
        var posicion = this.getPosicion();
        
         var that=this
        
         
        //creamos el control

        this.control = L.Routing.control({

            waypoints: [
        L.latLng(posicion[0], posicion[1]),
        L.latLng(destino)
    ],
          
            router: L.Routing.graphHopper('fe151d97-2701-4ca0-8e03-0429d1097447', {
             
                // hacemos una petición a graphhopper de modo que pueda elegir entre el mayor número de rutas posibles
                // el parámetro block_area descibe las coordenadas que la ruta esquivará  
            
                    urlParameters: {
                        vehicle: 'bike',
                        locale:"es",
                       "ch.disable":true,
                        algorithm: 'alternative_route',
                        max_paths: 30,
                        heading_penalty:1000,
                        min_plateau_factor:0,
                        max_exploration_factor:100,
                        max_weight_factor:5,
                        max_share_factor:0.9,
                        block_area: ''//'38.970867,-0.184796,100;38.973570,-0.173171,100;38.972836,-0.181836,100;38.973603,-0.173128,100;38.974781,-0.174969,100'
                    }
                    /*Block road access via a point with the format latitude,longitude 
                    or an area defined by a circle lat,lon,radius or a rectangle lat1,lon1,lat2,lon2. 
                    Separate several values with 
                    ;. Requires ch.disable=true.*/
                
            }),
            lineOptions : {
                addWaypoints: false,
                styles: [{color: 'green', opacity: 0.7, weight: 3}]
            },
             
            routeWhileDragging: false,
            collapsible:true,
            waypointNameFallback: function (latLng) {
                function zeroPad(n) {
                    n = Math.round(n);
                    return n < 10 ? '0' + n : n;
                }

                function sexagesimal(p, pos, neg) {
                    var n = Math.abs(p),
                        degs = Math.floor(n),
                        mins = (n - degs) * 60,
                        secs = (mins - Math.floor(mins)) * 60,
                        frac = Math.round((secs - Math.floor(secs)) * 100);
                    return (n >= 0 ? pos : neg) + degs + '°' +
                        zeroPad(mins) + '\'' +
                        zeroPad(secs) + '.' + zeroPad(frac) + '"';
                }

                return sexagesimal(latLng.lat, 'N', 'S') + ' ' + sexagesimal(latLng.lng, 'E', 'W');
            },
           
              // usamos el predictor de mapbox, ya que funciona en tiempo real

            geocoder: L.Control.Geocoder.mapbox('pk.eyJ1IjoidmljdG9yd3ciLCJhIjoiY2szOHRxeGlrMDFxejNlcHQ2enRnYzNlciJ9.eocW8NtO1Ar10si9Ne_2NA'),
          
        }).on('waypointschanged', function nuevaRuta(e) {
            
            //cuando cambian origen y/o destino pasamos dichos puntos a la función de la lógica que devuelve 
            //las medidas más cercanas y contaminadas a esquivar y se repite el enrutamiento cambiando el parametro de 
            //block_area por los puntos devueltos por la logica
        
            that.laLogicaFake.route(that.control.getWaypoints(), (puntos)=>{

    console.log(puntos)
           console.log(that.control.getWaypoints())
    
    
                that.control.getRouter().options.urlParameters.block_area = puntos;

                console.log( that.control.getRouter().options.urlParameters)


                var thet=that
              
                   controlAux=that.control;
                    that.control.route()
                  
                 
            })//laLogicaFake.route()
          
    }).on('routingerror', function (e) {
             
        // en caso de error, se repite el enrutamiento sin esquivar nada
        
           that.control=controlAux;
           that.control.getRouter().options.urlParameters.block_area = '';
           that.control.route()
          
           
        })
        
    }


    //--------------------------------------------------
    // crearCapas()
    //método que crea las capas en las que se añadirán las medidas y los basemaps
    //----------------------------------------------------------
    crearCapas() {

        //Creamos las capas:

        var capasBaseMapa = {
            "Calles": this.calles,
            "Satelite": this.satelite
        }

        var Capas = {
            "CO": this.CO,
            "NO3": this.NO3,
            "SO2": this.SO2,
            "O3": this.O3
           

        };
        L.control.layers(Capas,capasBaseMapa,{hideSingleBase: true}).addTo(this.mimapa);

    } //crearCapas()



}
