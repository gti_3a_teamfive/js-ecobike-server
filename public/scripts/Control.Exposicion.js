/* -------------------------------------------------------------------------- */
/*                          Control nivel exposición                          */
/* -------------------------------------------------------------------------- */

/**
 *  Leaflet Control Exposicion Plugin
 *  Autor: Joan Lluís Fuster
 *  
 *    Plugin para Leaflet que presenta una interfaz de usuario
 *    para seleccionar una ruta (diaria) y calcular el nivel de
 *    exposición a gases nocivos y muestra el resultado
 */

/**
 *  Support for AMD/CommonJS loaders to your Leaflet plugin
 *  https://github.com/Leaflet/Leaflet/blob/master/PLUGIN-GUIDE.md
 */

(function (factory, window) {

        // define an AMD module that relies on 'leaflet'
        if (typeof define === 'function' && define.amd) {
            define(['leaflet'], factory);

            // define a Common JS module that relies on 'leaflet'
        } else if (typeof exports === 'object') {
            module.exports = factory(require('leaflet'));
        }

        // attach your plugin to the global 'L' variable
        if (typeof window !== 'undefined' && window.L) {
            window.L.Control.Exposicion = factory(L);
            window.L.control.exposicion = function (options) {
                return new window.L.Control.Exposicion(options);
            };
        }
    }

    (function (L) {

        /* -------------------------------------------------------------------------- */
        /*                          Implementación del plugin                         */
        /* -------------------------------------------------------------------------- */

        /* ------------------ Almacena el contenido html del panel ------------------ */
        function getHtmlContent() {
            // ejemplo de string con el html: https://github.com/8to5Developer/leaflet-custom-searchbox/blob/master/src/leaflet.customsearchbox.js
            var controlHtmlContent = "<div><h3>Titulo<\/h3>Seleccionar dia: <input type=\"button\" id=\"button1\" class=\"my-button btn\" title=\"test\" value=\"click\" label=\"label\"\/><br><div id=\"resultadocontainer\"><div class=\"divTable unstyledTable\"><div class=\"divTableHeading\"><div class=\"divTableRow\"><div class=\"divTableHead\">Valores acumulados<\/div><\/div><\/div><div class=\"divTableBody\"><div class=\"divTableRow\"><div id=\"avgco\" class=\"divTableCell\">CO: 0 ppm<\/div><\/div><div class=\"divTableRow\"><div id=\"avgo3\" class=\"divTableCell\">O3: 0 ppm<\/div><\/div><div class=\"divTableRow\"><div id=\"avgno3\" class=\"divTableCell\">NO3: 0 ppm<\/div><\/div><div class=\"divTableRow\"><div id=\"avgso2\" class=\"divTableCell\">SO2: 0 ppm<\/div><\/div><\/div><\/div><\/div><\/div>";
            return controlHtmlContent;
        }
        /* -------------------------------------------------------------------------- */


        /* ---------------------- Dibuja un marcador en el mapa --------------------- */

        function dibujarMarcador(map, punto) {

            console.log('dibujar marcador llamado');

            icono = L.icon({
                iconUrl: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
                shadowUrl: 'http://maps.gstatic.com/mapfiles/shadow50.png',
            });

            var marker = L.marker([punto[0].lat, punto[0].lng], {
                    icon: icono
                }).bindPopup("<b> CO: </b>" + punto[0].value +
                    " ppb<br /><b> O3: </b>" + punto[1].value +
                    " ppb<br /><b> NO3: </b>" + punto[2].value +
                    " ppb<br /><b> SO2: </b>" + punto[3].value + " ppb")
                .openPopup()
                .addTo(map);

        }
        /* -------------------------------------------------------------------------- */


        /* ----------------------- Dibuja una curva en el mapa ---------------------- */

        function dibujarCurva(map, ruta) {

            console.log('dibujar curva llamado');

            var curva = [];

            // Dibuja una curva Bezier seleccionando uno de cada dos puntos del trayecto
            curva.push('M');
            curva.push([ruta[0].lat, ruta[0].lng]); // Punto inicial
            curva.push('Q');
            for (var i = 2; i < ruta.length - 2; i = i + 2) {
                curva.push([ruta[i].lat, ruta[i].lng]);
            }
            curva.push('T');
            curva.push([ruta[ruta.length - 1].lat, ruta[ruta.length - 1].lng]); // Punto final

            // Añade la curva al mapa
            var path = L.curve(curva, {
                animate: 2200
            }).addTo(map);

        }
        /* -------------------------------------------------------------------------- */


        /* --------------------- Interfaz de usuario del plugin --------------------- */

        /**
         *  Clase que hereda de Leaflet Control
         *  Puede recibir las opciones de tamaño, posicióon, etc en el constructor
         */

        var Exposicion = L.Control.extend({

            options: {
                position: 'bottomright',
                width: 200,
                height: 200
            },

            /* ------------------------------ Función onAdd ----------------------------- */

            /**
             *  Ejecutada al añadir el control al mapa
             *  Crea la interfaz de usuario
             */

            onAdd: function (map) {

                this._mainMap = map;
                var _this = this;

                // Crear un contenedor html
                var container = L.DomUtil.create('div', 'leaflet-control-exposicion');
                container.id = "controlcontainer";
                // Establece el tamaño
                container.style.width = this.options.width + 'px';
                container.style.height = this.options.height + 'px';

                // Genera el contenido html mediante JQuery
                $(container).html(getHtmlContent());

                // Añadir funcionalidad al html
                setTimeout(function () {

                    // Pulsar botón 1
                    $("#button1").click(function () {

                        console.log('buttonClicked');

                        // TODO: consulta que selecciona una ruta
                        var ruta = _this.crearRutaRandom();

                        // Obtiene las medidas de la ruta
                        _this.obtenerMedidas(ruta, map, function (medidas) {

                            // Calcula la media
                            var avg = [];
                            avg = _this.obtenerExposicionAcumulada(medidas);

                            // Muestra los resultados en el UI
                            $("#avgco").text("CO: " + avg[0] + " ppm");
                            $("#avgo3").text("O3: " + avg[1] + " ppm");
                            $("#avgno3").text("NO3: " + avg[2] + " ppm");
                            $("#avgso2").text("SO2: " + avg[3] + " ppm");

                        });

                        // Dibuja la ruta en el mapa
                        dibujarCurva(map, ruta);
                    });

                }, 1); // setTimeout()

                // desactiva eventos del mapa principal sobre el UI del plugin
                L.DomEvent.disableClickPropagation(container);
                L.DomEvent.on(container, 'mousewheel', L.DomEvent.stopPropagation);

                return container;
            },
            /* -------------------------------------------------------------------------- */


            /* ----------------------------- Obtener medidas ---------------------------- */

            /**
             *  Recibe los puntos de una ruta
             *  Pide la medida de cada punto de la ruta a la API REST
             *  Devuelve el resultado mediante un callback
             */

            obtenerMedidas: function (ruta, map, callback) {

                console.log('obtener medidas llamado');

                // TODO: cambiar a url servidor !!!
                urlServidor = "http://localhost:3000";
                var gasType = ["CO", "O3", "NO3", "SO2"];
                var elementsProcessed = 0;
                var resultado = [];

                ruta.forEach(function (element, index) {

                    var conjuntoMedida = [];

                    gasType.forEach(gas => {

                        var formBody = [];

                        for (var property in element) {
                            var encodedKey = encodeURIComponent(property);
                            var encodedValue = encodeURIComponent(element[property]);
                            formBody.push(encodedKey + "=" + encodedValue);
                        }

                        formBody.push("gasType=" + gas);

                        formBody = formBody.join("&");

                        fetch(urlServidor + "/interpolatedValue", {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                },
                                body: formBody
                            }).then(function (response) {
                                return response.json();
                            })
                            .then((res) => {

                                //console.log(res);
                                conjuntoMedida.push(res);

                                if (conjuntoMedida.length == 4) {

                                    resultado.push(conjuntoMedida);
                                    dibujarMarcador(map, conjuntoMedida);
                                    elementsProcessed++;

                                    // Llamada al callback cuando ha procesado todos los puntos
                                    if (elementsProcessed == ruta.length) {
                                        //console.log("RESULTADO: " + resultado);
                                        callback(resultado);
                                    }
                                }

                            }) // fetch()

                    }); // segundo foreach()

                }); // primer foreach()

            },
            /* -------------------------------------------------------------------------- */


            /* ----------------- Dibuja la ruta seleccionada en el mapa ----------------- */

            dibujarRuta: function (map, ruta) {

                console.log('dibujar ruta llamado');

                var pathOne = L.curve(ruta, {
                    animate: 3000
                }).addTo(map);

            },
            /* -------------------------------------------------------------------------- */


            /* --------------------- Calcula la exposición acumulada -------------------- */

            /**
             *  Función que recibe las medidas de una ruta
             *  Calcula la media ponderada por tiempo (ED)
             *  Compara la media con los límites diarios (VLA)
             *  Devuelve el valor de la media de cada gas
             */

            obtenerExposicionAcumulada: function (medidas) {

                console.log('obtener acumulado llamado');

                /**
                 *  LÍMITES DE EXPOSICIÓN: Valores límite ambientales
                 * 
                 *    http://bdlep.inssbt.es/LEP/index.jsp?nav=LEP
                 * 
                 *    CO:   VLA-ED: 20  ppm 23  mg/m3   VLA-EC: 100 ppm 117 mg/m3
                 *    O3:   VLA-ED: 0,1 ppm 0,2 mg/m3   VLA-EC: ---     ---
                 *    NO3:  VLA-ED: 50  ppm 92  mg/m3   VLA-EC: ---     ---
                 *    SO2:  VLA-ED: 0,5 ppm 1,32mg/m3   VLA-EC: 1   ppm 2,64mg/m3
                 * 
                 *    VLA:  valor límite ambiental
                 *    ED:   Exposición diaria --> media ponderada
                 *    EC:   Exposición corta (15 min) --> media ponderada
                 */

                var sum = [0, 0, 0, 0];
                var avg = [0, 0, 0, 0];

                for (var i = 0; i < medidas.length; i++) {

                    for (var j = 0; j < 4; j++) {
                        //console.log(medidas[i][j].value);
                        sum[j] += parseInt(medidas[i][j].value, 10) / 1000; //base 10, y dividido por 1000 para pasar a ppm
                    }

                }

                for (var i = 0; i < 4; i++) {
                    avg[i] = sum[i] / medidas.length;
                    console.log(avg[i]);
                }

                return avg;
            },
            /* -------------------------------------------------------------------------- */


            /* ---------------------- Devuelve una ruta para probar --------------------- */

            crearRutaRandom: function () {

                console.log('crear ruta random llamado');

                return [{
                        lat: "38.94812862803578",
                        lng: "-0.19157409667968753"
                    },
                    {
                        lat: "38.949196649538166",
                        lng: "-0.18565177917480472"
                    },
                    {
                        lat: "38.950665152828556",
                        lng: "-0.18084526062011722"
                    },
                    {
                        lat: "38.950932150158096",
                        lng: "-0.17621040344238284"
                    },
                    {
                        lat: "38.95220037373824",
                        lng: "-0.1709747314453125"
                    },
                    {
                        lat: "38.95400254715746",
                        lng: "-0.16711235046386722"
                    },
                    {
                        lat: "38.95657223369764",
                        lng: "-0.1698589324951172"
                    },
                    {
                        lat: "38.958808118900166",
                        lng: "-0.1731204986572266"
                    },
                    {
                        lat: "38.96107730338691",
                        lng: "-0.1780986785888672"
                    },
                    {
                        lat: "38.963279677655834",
                        lng: "-0.18436431884765628"
                    },
                    {
                        lat: "38.96771758751417",
                        lng: "-0.1878833770751953"
                    },
                    {
                        lat: "38.970587065430166",
                        lng: "-0.1850080490112305"
                    },
                    {
                        lat: "38.972155219352565",
                        lng: "-0.18148899078369143"
                    },
                    {
                        lat: "38.97452406688342",
                        lng: "-0.1765537261962891"
                    },
                    {
                        lat: "38.97972858033817",
                        lng: "-0.16681194305419925"
                    }
                ];

            },
            /* -------------------------------------------------------------------------- */


            /* -------------- Función llamada al quitar el plugin del mapa -------------- */

            onRemove: function (map) {
                // Nothing to do here
            }
            /* -------------------------------------------------------------------------- */

        });
        /* -------------------------------------------------------------------------- */


        /* -------------------------------------------------------------------------- */

        L.Map.mergeOptions({
            exposicionControl: false
        });
        /* -------------------------------------------------------------------------- */


        /* -------------------------------------------------------------------------- */

        L.Map.addInitHook(function () {
            if (this.options.exposicionControl) {
                this.exposicionControl = (new Exposicion()).addTo(this);
            }
        });
        /* -------------------------------------------------------------------------- */

        // return your plugin when you are done
        return Exposicion;

    }, window));
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */