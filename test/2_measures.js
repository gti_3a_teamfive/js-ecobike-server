/* -------------------------------------------------------------------------- */
/*                   Unit test for MEASURES API REST ROUTES                   */
/* -------------------------------------------------------------------------- */

// Require .env config
const dotenv = require("dotenv");
dotenv.config();

// Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let app = require('../app');
let User = require('../models/user');
let Measure = require('../models/measure');

let should = chai.should();

chai.use(chaiHttp);

/* ------------------------------ Parent block ------------------------------ */
describe('Measures API REST route', () => {
    beforeEach((done) => {
        //Before each test we empty the measures collection
        Measure.deleteMany({}, (err) => {
            done();
        });
    });

    /*
    * Test the /GET route
    */
    describe('/GET measures', () => {
        it('it should GET 0 measures', (done) => {
            chai.request(app)
                .get('/measures')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });
    });  // end of /GET route

    /*
    * Test the /POST route
    */
    describe('/POST measures', () => {
        it('it should NOT POST a measure without value field', (done) => {
            let user = {
                name: "John Lennon",
                email: "john@lennon.com"
            }
            let measure = {
                gasType: "Ozone"
            }
            chai.request(app)
                .post('/measures')
                .send(measure)
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('value');
                    res.body.errors.pages.should.have.property('kind').eql('required');
                    done();
                });
        });

        it('it should POST a measure ', (done) => {
            let user = new User({
                name: "Aretha Franklin",
                email: "aretha@voice.com"
            });

            user.save((err, user) => {
                let measure = {
                    gasType: "O3",
                    value: 1234,
                    longitude: 0.128,
                    latitude: 22.567,
                    user: user._id
                }

                chai.request(app)
                    .post('/measures')
                    .send(measure)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        //res.body.should.have.property('message').eql('Measure successfully added!');
                        res.body.should.have.property('gasType');
                        res.body.should.have.property('value');
                        res.body.should.have.property('date');
                        done();
                    });

            });

        });

    });  // end of /POST route

    /*
    * Test the /GET/:id route
    */
    describe('/GET/:id measure', () => {
        it('it should GET a measure by the given id', (done) => {

            let user2 = new User({
                name: "Fredie Mercury",
                email: "queen@google.com"
            });

            user2.save((err, user) => {
                let measure = new Measure({
                    gasType: "CO",
                    value: 102997,
                    location: {
                        type: "Point",
                        coordinates: [0.025, 29.027]
                    },
                    date: "2019-11-01T12:36:40.416Z",
                    user: user._id
                });

                measure.save((err, measure) => {
                    chai.request(app)
                        .get('/measures/' + measure._id)
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body.should.have.property('gasType');
                            res.body.should.have.property('value');
                            res.body.should.have.property('date');
                            res.body.should.have.property('_id');
                            //res.body.should.have.property('_id').eql(measure._id);
                            done();
                        });
                })
            });
        });
    });  // end of /GET/:id route

});  // end of block
