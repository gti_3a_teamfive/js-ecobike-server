/* -------------------------------------------------------------------------- */
/*                   Unit test for OFFICIAL MEASURES API REST ROUTES                   */
/* -------------------------------------------------------------------------- */

var MeasureOf = require('../models/officialMeasure');
var Measure = require('../models/measure');
// Require .env config
const dotenv = require("dotenv");
dotenv.config();

// Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let app = require('../app');



let should = chai.should();

chai.use(chaiHttp);

/* ------------------------------ Parent block ------------------------------ */
describe('officialMeasures API REST route', () => {
    beforeEach((done) => {
        //Before each test we empty the measures collection
        MeasureOf.deleteMany({}, (err) => {
            done();
        });
    });

    /*
    * Test the /GET route
    */
    describe('/GET officialmeasures', () => {
        it('it should be an array', (done) => {
            chai.request(app)
                .get('/officialMeasures')
                .end((err, res) => {
               

                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    console.log(res.body);
                    done();
                });
        });
    });  // end of /GET route
    /*
    * Test the /GET route
    */
    describe('/GET officialmeasures/position', () => {
        it('it should be the position of the station', (done) => {
            chai.request(app)
                .get('/officialMeasures/position')
                .end((err, res) => {
                 Measure.find({ 
    "location" : {
        "$near" : { 
            "$geometry" : { 
                "type" : "Point", 
                "coordinates" : [res.body.longitude, res.body.latitude] 
            } , 
            $maxDistance: 2000,
       $minDistance: 100
        } 
    } 
}).then(measure=>{console.log(" M E D I D A :"+measure)}).catch(err => {
                reject(err);
            })
                    res.should.have.status(200);
                    res.body.should.be.eql({"latitude":38.96888889,"longitude":0.19027778});
                    console.log(res.body);
                    done();
                });
        });
    });  // end of /GET route

    

});  // end of block
