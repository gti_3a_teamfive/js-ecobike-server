/* -------------------------------------------------------------------------- */
/*                      Populate database with fake data                      */
/* -------------------------------------------------------------------------- */

// Require .env config
const dotenv = require("dotenv");
dotenv.config();

// generates fake data
var faker = require('faker');
faker.locale = "es";

// Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let app = require('../app');
let User = require('../models/user');
let Measure = require('../models/measure');

let should = chai.should();

chai.use(chaiHttp);

/* ------------------------------ Parent block ------------------------------ */
describe('Populating database with fake data', () => {
    beforeEach((done) => { //Before each test we empty the database
        User.deleteMany({}, (err) => {
            Measure.deleteMany({}, (err) => {
                done();
            });
        });
    });

    /*
    * Database should be empty
    */
    describe('Database shold be empty', () => {

        it('it should GET 0 users', (done) => {
            chai.request(app)
                .get('/users')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });

        it('it should GET 0 measures', (done) => {
            chai.request(app)
                .get('/measures')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });

    });  // end of /GET route

    /*
    * Populate DB
    */
    describe('Creating users and measures', () => {

        it('Saving 10 users with 40 mesures each', (done) => {

            for (let i = 0; i < 10; i++) {

                let user = {
                    name: faker.name.findName(),
                    email: faker.internet.email()
                }

                chai.request(app)
                    .post('/users')
                    .send(user)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        //res.body.should.have.property('message').eql('User successfully added!');
                        res.body.should.have.property('name');
                        res.body.should.have.property('email');
                        res.body.should.have.property('_id');

                       /* // 40 medidas por usuario
                        for (let i = 0; i < 40; i++) {

                            let measure = {
                                gasType: faker.random.arrayElement(['CO', 'O3', 'NO3', 'SO2']),
                                value: faker.random.number(999999), // random number between 0 and 999999
                                temperature: faker.random.number({min: -4, max: 34}),
                                humidity: faker.random.number(100),
                                longitude: faker.finance.amount(-0.17, -0.19, 4),
                                latitude: faker.finance.amount(38.95, 38.98, 4),
                                date: faker.date.recent(),
                                userId: res.body._id
                            }

                            chai.request(app)
                                .post('/measures')
                                .send(measure)
                                .end((err, res) => {
                                    res.should.have.status(200);
                                    res.body.should.be.a('object');
                                    //res.body.should.have.property('message').eql('User successfully added!');
                                    res.body.should.have.property('gasType');
                                    res.body.should.have.property('value');
                                    res.body.should.have.property('location');
                                });

                        } // fin del for de medidas*/
                    

                    }); // fin POST /users

            } // fin del for de usuarios
            
            
            //creación del gradiente
        

                let user = {
                    name: faker.name.findName(),
                    email: faker.internet.email()
                }

                chai.request(app)
                    .post('/users')
                    .send(user)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        //res.body.should.have.property('message').eql('User successfully added!');
                        res.body.should.have.property('name');
                        res.body.should.have.property('email');
                        res.body.should.have.property('_id');

                   
                       let measure1 = {
                                gasType: 'CO',
                                value: 200000, //valores en aumento para ir de 10000 a 500000
                                temperature: faker.random.number({min: -4, max: 34}),
                                humidity: faker.random.number(100),
                                longitude: -0.18896341280196796,
                                latitude: 38.96 ,
                                date: faker.date.recent(),
                                userId: res.body._id
                            }

                            chai.request(app)
                                .post('/measures')
                                .send(measure1)
                                .end((err, res) => {
                                    res.should.have.status(200);
                                    res.body.should.be.a('object');
                                    //res.body.should.have.property('message').eql('User successfully added!');
                                    res.body.should.have.property('gasType');
                                    res.body.should.have.property('value');
                                    res.body.should.have.property('location');
                                });
                    
                 
                      let measure2 = {
                                gasType: 'CO',
                                value: 200000, //valores en aumento para ir de 10000 a 500000
                                temperature: faker.random.number({min: -4, max: 34}),
                                humidity: faker.random.number(100),
                                longitude: -0.18903136253356936,
                                latitude:  38.9782 ,
                                date: faker.date.recent(),
                                userId: res.body._id
                            }

                            chai.request(app)
                                .post('/measures')
                                .send(measure2)
                                .end((err, res) => {
                                    res.should.have.status(200);
                                    res.body.should.be.a('object');
                                    //res.body.should.have.property('message').eql('User successfully added!');
                                    res.body.should.have.property('gasType');
                                    res.body.should.have.property('value');
                                    res.body.should.have.property('location');
                                });
                    let measure3 = {
                                gasType: 'CO',
                                value: 200000, //valores en aumento para ir de 10000 a 500000
                                temperature: faker.random.number({min: -4, max: 34}),
                                humidity: faker.random.number(100),
                                longitude: -0.18896341280196796,
                                latitude: 38.975 ,
                                date: faker.date.recent(),
                                userId: res.body._id
                            }
                       chai.request(app)
                                .post('/measures')
                                .send(measure3)
                                .end((err, res) => {
                                    res.should.have.status(200);
                                    res.body.should.be.a('object');
                                    //res.body.should.have.property('message').eql('User successfully added!');
                                    res.body.should.have.property('gasType');
                                    res.body.should.have.property('value');
                                    res.body.should.have.property('location');
                                });
                    
                    // 50 medidas por usuario
                    var cont=20000;
                    var long=-0.1700;
                    var lat=38.9500;
                    var listaGases=['CO', 'O3', 'NO3', 'SO2']
                    for(let k=0; k<4; k++){
                    for(let j=0; j<10; j++){
                        for (let i = 0; i < 10; i++) {

                            let measure = {
                                gasType: listaGases[k],
                                value: cont+i*20000, //valores en aumento para ir de 10000 a 500000
                                temperature: faker.random.number({min: -4, max: 34}),
                                humidity: faker.random.number(100),
                                longitude: long-j*0.002+ k*0.003,
                                latitude: lat+i*0.003+ k*0.003,
                                date: faker.date.recent(),
                                userId: res.body._id
                            }

                            chai.request(app)
                                .post('/measures')
                                .send(measure)
                                .end((err, res) => {
                                    res.should.have.status(200);
                                    res.body.should.be.a('object');
                                    //res.body.should.have.property('message').eql('User successfully added!');
                                    res.body.should.have.property('gasType');
                                    res.body.should.have.property('value');
                                    res.body.should.have.property('location');
                                });

                        } //fin del for de las latitudes por cada longitud
                        
                    }// fin del for de medidas
                        
                    }// fin del for de los gases
                  

                    }); // fin POST /users

          

            done();

        }); // fin it()

    });  // end of Populate DB

});  // end of block
