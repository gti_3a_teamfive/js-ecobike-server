/* -------------------------------------------------------------------------- */
/*                     Unit test for USERS API REST ROUTES                    */
/* -------------------------------------------------------------------------- */

// Require .env config
const dotenv = require("dotenv");
dotenv.config();

// Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let app = require('../app');
let User = require('../models/user');

let should = chai.should();

chai.use(chaiHttp);

/* ------------------------------ Parent block ------------------------------ */
describe('Users API REST route', () => {
    beforeEach((done) => {
        //Before each test we empty the users collection
        User.deleteMany({}, (err) => {
            done();
        });
    });

    /*
    * Test the /GET route
    */
    describe('/GET users', () => {
        it('it should GET 0 users', (done) => {
            chai.request(app)
                .get('/users')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });
    });  // end of /GET route

    /*
    * Test the /POST route
    */
    describe('/POST users', () => {
        it('it should NOT POST a user without email field', (done) => {
            let user = {
                name: "John Lennon"
            }
            chai.request(app)
                .post('/users')
                .send(user)
                .end((err, res) => {
                    //res.should.have.status(200);
                    res.should.have.status(500);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('email');
                    res.body.errors.pages.should.have.property('kind').eql('required');
                    done();
                });
        });

        it('it should NOT POST a user without name field', (done) => {
            let user = {
                email: "none@example.com"
            }
            chai.request(app)
                .post('/users')
                .send(user)
                .end((err, res) => {
                    //res.should.have.status(200);
                    res.should.have.status(500);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('name');
                    res.body.errors.pages.should.have.property('kind').eql('required');
                    done();
                });
        });

        it('it should POST a user ', (done) => {
            let user = {
                name: "Michael Jackson",
                email: "bad@neverland.com"
            }

            chai.request(app)
                .post('/users')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    //res.body.should.have.property('message').eql('User successfully added!');
                    res.body.should.have.property('name');
                    res.body.should.have.property('email');
                    done();
                });
        });

    });  // end of /POST route

    /*
    * Test the /GET/:id route
    */
    describe('/GET/:id user', () => {
        it('it should GET a user by the given id', (done) => {

            let user = new User({
                name: "George Michael",
                email: "faith@george.com"
            });

            user.save((err, user) => {
                chai.request(app)
                    .get('/users/' + user._id)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('name');
                        res.body.should.have.property('email');
                        res.body.should.have.property('_id');
                        //res.body.should.have.property('_id').eql(user._id);
                        done();
                    });
            });

        });
    });  // end of /GET/:id route

});  // end of block
