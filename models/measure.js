/* -------------------------------------------------------------------------- */
/*                   Measure model schema for mongo database                  */
/* -------------------------------------------------------------------------- */

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var measureSchema = new Schema({
  gasType: {
    type: String, enum:
      ['CO', 'O3', 'NO3', 'SO2'],
    required: true
  },
  value: { type: Number, required: true },
  temperature: { type: Number },
  humidity: { type: Number },
  location: {
    type: {
      type: String,
      enum: ['Point', 'LineString', 'Polygon'],
      required: true
    },
    coordinates: {
      type: [Number],
      required: true
    }
  },
  date: {
    type: Date,
    required: true,
    default: Date.now
  },
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
});

measureSchema.index({ location: '2dsphere' });   // index for geo queries

module.exports = mongoose.model('Measure', measureSchema);