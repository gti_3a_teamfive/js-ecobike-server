/* -------------------------------------------------------------------------- */
/*                   Measure model schema for mongo database                  */
/* -------------------------------------------------------------------------- */

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var userSchema = new Schema({
  name: { type: String, required: true },
  email: { type: String, required: true, unique: true },
});

module.exports = mongoose.model('User', userSchema);
