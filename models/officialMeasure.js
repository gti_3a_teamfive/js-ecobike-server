/* -------------------------------------------------------------------------- */
/*                                officialMeasure model                               */
/* -------------------------------------------------------------------------- */

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var measureSchema = new Schema({
  
   
  so2: { type: Number, required: true },
  no2: { type: Number, required: true },
  co: { type: Number, required: true },
  no: { type: Number, required: true },
  nox: { type: Number, required: true },
  o3: { type: Number, required: true },
  date: {
    type: String,
    required: true,
    default: Date.now
  },
  location: {
    type: {
      type: String,
      enum: ['Point', 'LineString', 'Polygon'],
      required: true
    },
    coordinates: {
      type: [Number],
      required: true
    }
  }


});

measureSchema.index({ location: '2dsphere' });
module.exports = mongoose.model('officialMeasure', measureSchema);
