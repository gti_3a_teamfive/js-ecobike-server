/* -------------------------------------------------------------------------- */
/*                   interpolatedValue model schema for mongo database                  */
/* -------------------------------------------------------------------------- */

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var interpolatedValueSchema = new Schema({
  gasType: {
    type: String, enum:
      ['CO', 'O3', 'NO3', 'SO2', '*'],
    required: true
  },
  value: { type: Number, required: true },

     location: {
    type: {
      type: String,
      enum: ['Point', 'LineString', 'Polygon'],
      required: true
    },
    coordinates: {
      type: [Number],
      required: true
    }
  }
  });
  


interpolatedValueSchema.index({ location: '2dsphere' });   // index for geo queries

module.exports = mongoose.model('interpolatedValue', interpolatedValueSchema);