/* -------------------------------------------------------------------------- */
/*                           /createRoutes REST Routes                            */
/* -------------------------------------------------------------------------- */

var express = require('express');
const bodyParser = require('body-parser')
var https = require("https");
var router = express.Router();
var bl = require('./../businessLogic');
var Measure = require('../models/measure');
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const mapboxToken= 'pk.eyJ1IjoidmljdG9yd3ciLCJhIjoiY2szOHRxeGlrMDFxejNlcHQ2enRnYzNlciJ9.eocW8NtO1Ar10si9Ne_2NA'

/* ---------------------------- POST createRoutes ---------------------------- */
router.post('/', (req, res, next) => {

   

    //allow localhost request for testing, comment later
    res.setHeader('Access-Control-Allow-Origin', '*');
    //--------------------------------------------------------------------

    
    
    var midPoint= middlePoint(req.body.latS,req.body.lngS,req.body.latE,req.body.lngE);
    
    //We look for the nearby measures of the middle point between the route's start and end
    Measure.find({
      location: {
          $near: {
             // $maxDistance:1000000,// getDistanceFromLatLonInKm(midPoint[1],midPoint[0],req.body.latS, req.body.lngS)*1000,
              $minDistance: 10,
              
               $geometry:{"type": "Point",
               "coordinates": [midPoint[1],midPoint[0]]}                      
          }
      }
  }).then(nearbyMeasures => {

  // measures that are much further from the midpoint that the start or those which are too close to the start or end are discarded

nearbyMeasures.forEach((element)=>{

  let lat=element.location.coordinates[1];
  let lng=element.location.coordinates[0];
  if(getDistanceFromLatLonInKm(midPoint[1],midPoint[0],lat, lng)+0.1>getDistanceFromLatLonInKm(midPoint[1],midPoint[0],req.body.latS, req.body.lngS)){
  let index = nearbyMeasures.indexOf(element);
  if (index > -1) {
    nearbyMeasures.splice(index, 1);
  }
}
  if(getDistanceFromLatLonInKm(req.body.latS, req.body.lngS,lat, lng)*1000<300||getDistanceFromLatLonInKm(req.body.latE, req.body.lngE,lat, lng)*1000<300){
    let index = nearbyMeasures.indexOf(element);
    if (index > -1) {
      nearbyMeasures.splice(index, 1);
    }
}

})
 

 // we order the measures starting from the most polluted ones and ending on the least polluted ones
    nearbyMeasures.sort(function(a, b){
        
      return b.value-a.value;
      
  })//sort
 
  var mostPollutedWaypointsLength=50;

  //if less than 50 nearby pollluted measures are found, the number of measures to avoid is reduced
  if(nearbyMeasures.length<50){mostPollutedWaypointsLength=nearbyMeasures.length}
  
 var mostPollutedString='';
  
  //if start and end are less than 2 km from each other, the number of measures to avoid reduces 
 if((getDistanceFromLatLonInKm(req.body.latE, req.body.lngE,req.body.latS, req.body.lngS)<2)&&nearbyMeasures.length>=5){
mostPollutedWaypointsLength=3;
 }
  
  //we populate a string with the location of the most polluted measures with a radius of 200m which will be avoided
  for(let i=0; i<mostPollutedWaypointsLength; i++){
   
    mostPollutedString += nearbyMeasures[i].location.coordinates[1].toString()+','+nearbyMeasures[i].location.coordinates[0].toString()+',200;'
    
    
  }
    
  //the final string with the avoidable circles is sent without the last ";"
  res.json(mostPollutedString.slice(0,-1));
  
  })//Mongodb 
  
     });

//-----------------------------------------------------------------
// degrees: R--> drg2rad() --> radians:R
// author: Víctor
//-------------------------------------------------------------------
//-- Define radius function
 function toRad(num) {
      return num * Math.PI / 180;
  }

//-----------------------------------------------------------------
// radians: R--> drg2rad() --> degrees:R
// author: Víctor
//-------------------------------------------------------------------
//-- Define degrees function
function toDeg(num) {
      return num * (180 / Math.PI);
  }


  //-----------------------------------------------------------------
// lat1: R,lng1: R,lat2: R, lng2: R --> middlePoint() --> latMidPoint:R, lngMidPoint:R
// author: Víctor
//-------------------------------------------------------------------

//-- Define middle point function
function middlePoint(lat1, lng1, lat2, lng2) {


  //-- Longitude difference
  var dLng = toRad((lng2 - lng1));

  //-- Convert to radians
  lat1 = toRad(lat1);
  lat2 = toRad(lat2);
  lng1 = toRad(lng1);

  var bX = Math.cos(lat2) * Math.cos(dLng);
  var bY = Math.cos(lat2) * Math.sin(dLng);
  var lat3 = Math.atan2(Math.sin(lat1) + Math.sin(lat2), Math.sqrt((Math.cos(lat1) + bX) * (Math.cos(lat1) + bX) + bY * bY));
  var lng3 = lng1 + Math.atan2(bY, Math.cos(lat1) + bX);

  //-- Return result
  return [toDeg(lng3), toDeg(lat3)];
}

//---------------------------------------------------------------
// lat1: R, lng1:R, lat2:r, lng2:R -->getDistanceFromLatLonInKm()-->distance:R
// author: Víctor
//----------------------------------------------------------------
function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = toRad(lat2 - lat1); // deg2rad below
  var dLon = toRad(lon2 - lon1);
  var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return d;
}




//------------------------------------------------------------------------------------------------------------



module.exports = router;
