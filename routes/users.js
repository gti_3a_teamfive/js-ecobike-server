/* -------------------------------------------------------------------------- */
/*                             /Users REST Routes                             */
/* -------------------------------------------------------------------------- */

var express = require('express');
var router = express.Router();
var bl = require('./../businessLogic');

/* ------------------------------ GET all users ----------------------------- */
router.get('/', (req, res, next) => {
  bl.readAllUsers()    // call to logic
    .then(result => {
      // do something with result
      return result
    })
    .then(users => res.json(users))
    .catch(next)
});
/* -------------------------------------------------------------------------- */

/* ------------------------------ Get one user ------------------------------ */
router.get('/:userId', (req, res, next) => {
  bl.readUser(req.params.userId)
  .then(result => {
    // do something with result
    return result
  })
  .then(user => res.json(user))
  .catch(next)
});
/* -------------------------------------------------------------------------- */

/* ----------------------------- Create one user ---------------------------- */
router.post('/', (req, res, next) => {
  bl.createUser(req.body)
  .then(result => {
    // do something with result
    return result
  })
    //.then(user => res.status(201).json(user))
  .then(user => res.json(user))
  .catch(next)
});
/* -------------------------------------------------------------------------- */

/* ----------------------------- Update one user ---------------------------- */
router.patch('/:userId', function (req, res, next) {
  res.json({
    status: 'API Its Working',
    message: 'Welcome to RESTHub crafted with love!'
  });
});
/* -------------------------------------------------------------------------- */

/* ----------------------------- Delete one user ---------------------------- */
router.delete('/:userId', function (req, res, next) {
  res.json({
    status: 'API Its Working',
    message: 'Welcome to RESTHub crafted with love!'
  });
});
/* -------------------------------------------------------------------------- */

module.exports = router;
