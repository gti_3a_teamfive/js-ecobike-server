/* -------------------------------------------------------------------------- */
/*                           /Measures REST Routes                            */
/* -------------------------------------------------------------------------- */

var express = require('express');
var router = express.Router();
var bl = require('./../businessLogic');

/* ---------------------------- GET all measures ---------------------------- */
router.get('/', (req, res, next) => {

  //allow localhost request for testing, comment later
  res.setHeader('Access-Control-Allow-Origin', '*');
  //--------------------------------------------------------------------

  bl.readAllMeasures()    // call to logic
    .then(result => {
      // do something with result
      return result
    })
    .then(measures => res.json(measures))
    .catch(next)
});
/* -------------------------------------------------------------------------- */

/* ------------------------------ Get one measure --------------------------- */
router.get('/:measureId', (req, res, next) => {
  bl.readMeasure(req.params.measureId)
    .then(result => {
      // do something with result
      return result
    })
    .then(measure => res.json(measure))
    .catch(next)
});
/* -------------------------------------------------------------------------- */

/* ----------------------------- Create one measure ------------------------- */
router.post('/', (req, res, next) => {
  bl.createMeasure(req.body)
    .then(result => {
      // do something with result
      return result
    })
    .then(measure => res.json(measure))
    .catch(next)
});
/* -------------------------------------------------------------------------- */

/* ----------------------------- Update one measure ------------------------- */
router.patch('/:measureId', function (req, res, next) {
  res.json({
    status: 'API Its Working',
    message: 'Welcome to RESTHub crafted with love!'
  });
});
/* -------------------------------------------------------------------------- */

/* --------------------------- Delete one measure --------------------------- */
router.delete('/:measureId', function (req, res, next) {
  res.json({
    status: 'API Its Working',
    message: 'Welcome to RESTHub crafted with love!'
  });
});
/* -------------------------------------------------------------------------- */

module.exports = router;
