/* -------------------------------------------------------------------------- */
/*                           /interpolatedValue REST Routes                            */
// interpolatedValue.js
// method that returns the interpolated value of a given coordinate
// team 5
// author: Víctor Blanco Bataller
// 3/11/2019
// copyright
/* -------------------------------------------------------------------------- */

var express = require('express');
const bodyParser = require('body-parser')
var router = express.Router();
var bl = require('./../businessLogic');

/* ---------------------------- GET interpolated value ---------------------------- */
router.post('/', (req, res, next) => {

   

    //allow localhost request for testing, comment later
    res.setHeader('Access-Control-Allow-Origin', '*');
    //--------------------------------------------------------------------

   // res.json(req.body)
    
    bl.interpolatedValue(req.body.lat, req.body.lng, req.body.gasType) // call to logic
        .then(result => {
            // do something with result

            return result
        })
        .then(value => {
        if(value.value==null){value.value=0}
        value.value= Math.round(value.value)
            res.json(value)
        })
        .catch(next)




});
/* -------------------------------------------------------------------------- */



//------------------------------------------------------------------------------------------------------------

module.exports = router;
