/* -------------------------------------------------------------------------- */
/*                           /OfficialMeasures REST Routes                            */
/* -------------------------------------------------------------------------- */

var express = require('express');
var router = express.Router();
var bl = require('./../businessLogic');

/* ---------------------------- GET all official measures ---------------------------- */
router.get('/', (req, res, next) => {

    res.setHeader('Access-Control-Allow-Origin', '*');

    //allow localhost request for testing, comment later
    res.setHeader('Access-Control-Allow-Origin', '*');
    //--------------------------------------------------------------------

    bl.officialMeasures() // call to logic
        .then(result => {
            // do something with result

            return result
        })
        .then(measures => {
            res.json(measures)
        })
        .catch(next)




});
/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */

/* ----------------------------- Obtain position from Gandia measurement station ------------------------- */
router.get('/position', function (req, res, next) {

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.json({
        latitude: 38.96888889,

        longitude: -0.19027778
    });
});

/* -------------------------------------------------------------------------- */
//-------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------

module.exports = router;
